This is a transcriber that can use speech recognition to construct text simple text documents. This can be used as a simple text editor as it has the features to load and store files as well as edit the text along with spoken input.

# HOW TO RUN

to run the transcriber there are a lot of indvidual packages that need to be downloaded seperately

To make it easier to run I have set up a python environment with all necessary dependencies in place

You will need to activate this environment before being able to run the code

to do this, open up a terminal and use the following command from the smae directory that this README file is in:

./Scripts/activate	(use backslashes in case of Windows)

once this is set up you will need to move to the transcriber directory wfich is found along side this read me file. 

Here you will find a file called SSTTranscriber.py

to be able to run this code you need to give it a model as input, optionally you can use the default scorer, but I have included a link to an external scorer : https://github.com/mozilla/DeepSpeech/releases/download/v0.9.1/deepspeech-0.9.1-models.scorer
beware that the external scorer is quite large download so it could be better to do without it.

The model required is provided within the deepspeech_models directory to run the code just use the command:

python ./SSTTranscriber.py -m ./deepspeech_models/deepspeech-0.8.0-models.pbmm

you can also download the lastest models from https://github.com/mozilla/DeepSpeech/releases