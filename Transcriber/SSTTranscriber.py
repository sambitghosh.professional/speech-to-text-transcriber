import time, logging
from datetime import datetime
import threading, collections, queue, os, os.path
import deepspeech
import numpy as np
import pyaudio
import wave
import webrtcvad
from halo import Halo
from scipy import signal
from tkinter import *
from tkinter import scrolledtext
from tkinter import filedialog


CloseFlag = False
Listen = False

ARGSGlobal = None


# This class was referenced from Mozilla Deepspeech examples Github repository code mic_vad_streaming.py : https://github.com/mozilla/DeepSpeech-examples/tree/r0.8/mic_vad_streaming
class Audio(object):
    """Streams raw audio from microphone. Data is received in a separate thread, and stored in a buffer, to be read from."""

    FORMAT = pyaudio.paInt16
    # Network/VAD rate-space
    RATE_PROCESS = 16000
    CHANNELS = 1
    BLOCKS_PER_SECOND = 50

    def __init__(self, callback=None, device=None, input_rate=RATE_PROCESS, file=None):
        def proxy_callback(in_data, frame_count, time_info, status):
            #pylint: disable=unused-argument
            if self.chunk is not None:
                in_data = self.wf.readframes(self.chunk)
            callback(in_data)
            return (None, pyaudio.paContinue)
        if callback is None: callback = lambda in_data: self.buffer_queue.put(in_data)
        self.buffer_queue = queue.Queue()
        self.device = device
        self.input_rate = input_rate
        self.sample_rate = self.RATE_PROCESS
        self.block_size = int(self.RATE_PROCESS / float(self.BLOCKS_PER_SECOND))
        self.block_size_input = int(self.input_rate / float(self.BLOCKS_PER_SECOND))
        self.pa = pyaudio.PyAudio()

        kwargs = {
            'format': self.FORMAT,
            'channels': self.CHANNELS,
            'rate': self.input_rate,
            'input': True,
            'frames_per_buffer': self.block_size_input,
            'stream_callback': proxy_callback,
        }

        self.chunk = None
        # if not default device
        if self.device:
            kwargs['input_device_index'] = self.device
        elif file is not None:
            self.chunk = 320
            self.wf = wave.open(file, 'rb')

        self.stream = self.pa.open(**kwargs)
        self.stream.start_stream()

    def resample(self, data, input_rate):
        """
        Microphone may not support our native processing sampling rate, so
        resample from input_rate to RATE_PROCESS here for webrtcvad and
        deepspeech

        Args:
            data (binary): Input audio stream
            input_rate (int): Input audio rate to resample from
        """
        data16 = np.fromstring(string=data, dtype=np.int16)
        resample_size = int(len(data16) / self.input_rate * self.RATE_PROCESS)
        resample = signal.resample(data16, resample_size)
        resample16 = np.array(resample, dtype=np.int16)
        return resample16.tostring()

    def read_resampled(self):
        """Return a block of audio data resampled to 16000hz, blocking if necessary."""
        return self.resample(data=self.buffer_queue.get(),
                             input_rate=self.input_rate)

    def read(self):
        """Return a block of audio data, blocking if necessary."""
        return self.buffer_queue.get()

    def destroy(self):
        self.stream.stop_stream()
        self.stream.close()
        self.pa.terminate()

    frame_duration_ms = property(lambda self: 1000 * self.block_size // self.sample_rate)

    def write_wav(self, filename, data):
        logging.info("write wav %s", filename)
        wf = wave.open(filename, 'wb')
        wf.setnchannels(self.CHANNELS)
        # wf.setsampwidth(self.pa.get_sample_size(FORMAT))
        assert self.FORMAT == pyaudio.paInt16
        wf.setsampwidth(2)
        wf.setframerate(self.sample_rate)
        wf.writeframes(data)
        wf.close()


# This class was referenced from Mozilla Deepspeech examples Github repository code mic_vad_streaming.py : https://github.com/mozilla/DeepSpeech-examples/tree/r0.8/mic_vad_streaming
class VADAudio(Audio):
    """Filter & segment audio with voice activity detection."""

    def __init__(self, aggressiveness=3, device=None, input_rate=None, file=None):
        super().__init__(device=device, input_rate=input_rate, file=file)
        self.vad = webrtcvad.Vad(aggressiveness)

    def frame_generator(self):
        """Generator that yields all audio frames from microphone."""
        if self.input_rate == self.RATE_PROCESS:
            while True:
                yield self.read()
        else:
            while True:
                yield self.read_resampled()

    def vad_collector(self, padding_ms=300, ratio=0.75, frames=None):
        """Generator that yields series of consecutive audio frames comprising each utterence, separated by yielding a single None.
            Determines voice activity by ratio of frames in padding_ms. Uses a buffer to include padding_ms prior to being triggered.
            Example: (frame, ..., frame, None, frame, ..., frame, None, ...)
                      |---utterence---|        |---utterence---|
        """
        if frames is None: frames = self.frame_generator()
        num_padding_frames = padding_ms // self.frame_duration_ms
        ring_buffer = collections.deque(maxlen=num_padding_frames)
        triggered = False

        for frame in frames:
            if len(frame) < 640:
                return

            is_speech = self.vad.is_speech(frame, self.sample_rate)

            if not triggered:
                ring_buffer.append((frame, is_speech))
                num_voiced = len([f for f, speech in ring_buffer if speech])
                if num_voiced > ratio * ring_buffer.maxlen:
                    triggered = True
                    for f, s in ring_buffer:
                        yield f
                    ring_buffer.clear()

            else:
                yield frame
                ring_buffer.append((frame, is_speech))
                num_unvoiced = len([f for f, speech in ring_buffer if not speech])
                if num_unvoiced > ratio * ring_buffer.maxlen:
                    triggered = False
                    yield None
                    ring_buffer.clear()


def close_window():
    global CloseFlag
    CloseFlag = True

def read_audio():

    if os.path.isdir(ARGSGlobal.model):
        model_dir = ARGSGlobal.model
        ARGSGlobal.model = os.path.join(model_dir, 'output_graph.pb')
        ARGSGlobal.scorer = os.path.join(model_dir, ARGSGlobal.scorer)

    model = deepspeech.Model(ARGSGlobal.model)
    if ARGSGlobal.scorer:
        logging.info("ARGS.scorer: %s", ARGSGlobal.scorer)
        model.enableExternalScorer(ARGSGlobal.scorer)

    vad_audio = VADAudio(aggressiveness=ARGSGlobal.vad_aggressiveness,
                         device=ARGSGlobal.device,
                         input_rate=ARGSGlobal.rate,
                         file=ARGSGlobal.file)
    text = ""
    frames = vad_audio.vad_collector()
    print(type(frames))
    # Stream from microphone to DeepSpeech using VAD
    spinner = None
    if not ARGSGlobal.nospinner:
        spinner = Halo(spinner='line')
    stream_context = model.createStream()
    wav_data = bytearray()

    for frame in frames:
        if frame is not None:
            if spinner: spinner.start()
            logging.debug("streaming frame")
            stream_context.feedAudioContent(np.frombuffer(frame, np.int16))
            if ARGSGlobal.savewav: wav_data.extend(frame)
            time.sleep(0.01)
        else:
            # if there is not frame of spoken audio coming we can end the current stream and process the data.
            if spinner: spinner.stop()
            logging.debug("end utterence")
            text = stream_context.finishStream()
            frames.close();
            vad_audio.destroy()
            break
    return text


def toggle_button():
    global Listen
    if Listen:
        Listen=False
    else:
        Listen=True


def start_listening(gui):
    global Listen

    if Listen:
        gui.update_button("Listening...\nStop Listening")
        text = read_audio()
        print("Recognized: %s" % text)
        text = str.capitalize(text)
        text = " " + text + "."
        gui.recognised_text(text)
        gui.update_entry(text)
    else:
        gui.update_button("Start Listening")


class MenuBar:

    def __init__(self, parent):
        font_specs = ("ubuntu", 14)

        menu = Menu(parent.root, font=font_specs)
        parent.root.config(menu=menu)

        file_dropdown = Menu(menu, font=font_specs, tearoff=0)
        file_dropdown.add_command(label="New File",
                                  command=parent.New_file)
        file_dropdown.add_command(label="Open File",
                                  command=parent.Open_file)
        file_dropdown.add_command(label="Save",
                                  command=parent.Save)
        file_dropdown.add_command(label="Save As",
                                  command=parent.Save_as)

        menu.add_cascade(label="File", menu=file_dropdown)


class GUI_Handler(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)

        self.start()


    def callback(self):
        close_window()
        self.root.quit()



    def run(self):
        self.root = Tk()
        font_specs = ("ubuntu", 14)
        self.ListeningButton = Button(self.root, text='Start Listening', padx=30, pady=30, command=toggle_button,font=font_specs)
        self.TextEntry = scrolledtext.ScrolledText(self.root, font=font_specs)
        self.RecognisedLabel = Label(self.root, text="Nothing recognized", padx=10, pady=10)
        self.root.protocol("WM_DELETE_WINDOW", self.callback)

        self.filename = None

        self.RecognisedLabel.grid(row=0, column=0)
        self.ListeningButton.grid(row=2, column=0)

        self.TextEntry.grid(row=1, column=0, sticky=E + W + S + N)

        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(1, weight=1)

        self.menu_bar = MenuBar(self)

        self.root.mainloop()

    def recognised_text(self,text):
        self.RecognisedLabel.configure(text=("Recognized words: " + text))

    def update_entry(self,text):
        self.TextEntry.insert(INSERT, text)

    def update_button(self,text):
        #self.StartListeningButton.configure(text=text)
        self.ListeningButton.configure(text = text)

    def set_title(self, name=None):
        if name:
            self.root.title(name + " - Transcriber")
        else:
            self.root.title("Untitled - Transcriber")

    def Save_as(self):
        try:
            new_file = filedialog.asksaveasfilename(
                initialfile="Untitled.txt",
                defaultextension=".txt",
                filetypes=[("All Files", "*.*"),
                           ("Text Files", "*.txt"),
                           ("Markdown Documents", "*.md")])
            text_input = self.TextEntry.get(1.0, END)
            f=open(new_file, "w")
            f.write(text_input)
            f.close()
            self.filename = new_file
            self.set_title(self.filename)
        except Exception as e:
            print(e)

    def Save(self):
        if self.filename:
            try:
                text_input = self.TextEntry.get(1.0, END)
                f = open(self.filename, "w")
                f.write(text_input)
                f.close()
            except Exception as e:
                print(e)
        else:
            self.Save_as()

    def New_file(self):
        self.TextEntry.delete(1.0, tk.END)
        self.filename = None
        self.set_title()

    def Open_file(self):
        self.filename = filedialog.askopenfilename(
            defaultextension=".txt",
            filetypes=[("All Files", "*.*"),
                       ("Text Files", "*.txt"),
                       ("Markdown Documents", "*.md")])
        if self.filename:
            self.TextEntry.delete(1.0, tk.END)
            f = open(self.filename, "r")
            self.TextEntry.insert(1.0, f.read())
            f.close()
            self.set_window_title(self.filename)


if __name__ == '__main__':
    DEFAULT_SAMPLE_RATE = 16000

    import argparse
    parser = argparse.ArgumentParser(description="Stream from microphone to DeepSpeech using VAD")

    parser.add_argument('-v', '--vad_aggressiveness', type=int, default=3,
                        help="Set aggressiveness of VAD: an integer between 0 and 3, 0 being the least aggressive about filtering out non-speech, 3 the most aggressive. Default: 3")
    parser.add_argument('--nospinner', action='store_true',
                        help="Disable spinner")
    parser.add_argument('-w', '--savewav',
                        help="Save .wav files of utterences to given directory")
    parser.add_argument('-f', '--file',
                        help="Read from .wav file instead of microphone")

    parser.add_argument('-m', '--model', required=True,
                        help="Path to the model (protocol buffer binary file, or entire directory containing all standard-named files for model)")
    parser.add_argument('-s', '--scorer',
                        help="Path to the external scorer file.")
    parser.add_argument('-d', '--device', type=int, default=None,
                        help="Device input index (Int) as listed by pyaudio.PyAudio.get_device_info_by_index(). If not provided, falls back to PyAudio.get_default_device().")
    parser.add_argument('-r', '--rate', type=int, default=DEFAULT_SAMPLE_RATE,
                        help=f"Input device sample rate. Default: {DEFAULT_SAMPLE_RATE}. Your device may require 44100.")

    ARGS = parser.parse_args()
    ARGSGlobal = parser.parse_args()
    if ARGS.savewav: os.makedirs(ARGS.savewav, exist_ok=True)

gui = GUI_Handler()

# wait for gui to start up
time.sleep(1)
while not CloseFlag:
    # yield for the gui mainloop to get a chance
    time.sleep(0.01)
    start_listening(gui)
    if gui.root == None:
        break


